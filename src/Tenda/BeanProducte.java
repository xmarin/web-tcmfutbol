package Tenda;

import java.util.ArrayList;
import java.util.HashMap;

public class BeanProducte {
	
	private TendaBBDD persistencia;
	private String familia;
	private String usuari, password;
	private int codiProducte;
	private HashMap<Integer, String[]> productesPerFamilia;
	private HashMap<Integer, String[]> productesCercar;
	private String cercar;
	private int numFactura;
	
	public BeanProducte(){
		persistencia = new TendaBBDD();
	}
	
	public String getUsuari() {
		return this.usuari;
	}

	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsuari(String usuari) {
		this.usuari = usuari;
	}
	
	public int getNumFactura(){
		return numFactura;
	}
	
	public void setNumFactura(int num){
		numFactura = num;
	}

	//usuari
	public boolean existeixUser(String user, String pass){
		if (persistencia.existeixUser(user, pass).equals("OK")) return true;
		else return false;
	}
	
	public boolean insertUser(String nomReg, String cogReg, String usuReg,
			String passReg, String pobReg, String telReg, String nifReg,
			String emaReg) {
		if(persistencia.insertUser(nomReg,cogReg,usuReg,passReg,pobReg,telReg,nifReg,emaReg).equals("OK")) return true;
		else return false;
	}
	
	public boolean esAdmin(String usuari) {
		if(persistencia.esAdmin(usuari).equals("OK")) return true;
		else return false;
	}
	
	//producte
	public ArrayList getFamiliasBBDD() {
		return persistencia.getFamiliasArrayList();
	}
	
	public ArrayList getProductesBBDD() {
		return persistencia.getProductesArrayList();
	}

	public String getFamilia(){
		return familia;
	}
	
	public void setFamilia(String f){
		familia = f;
	}

	public HashMap<Integer,String[]> getCerca() throws Exception{
		this.productesCercar = persistencia.getProductesCercar(cercar);
		return productesCercar;
	}
	
	public HashMap<Integer,String[]> getProductes() throws Exception{
		this.productesPerFamilia = persistencia.getProductesPerFamilia(familia);
		return productesPerFamilia;
	}
	
	public HashMap<Integer,String[]> getCistella(){
		return persistencia.getCistella(usuari);
	}

	public int insertProducte(String nomFam, String nomPro, String descPro, double preuPro, int stockPro, String imaPro) {
		return persistencia.insertProducte(nomFam,nomPro,descPro,preuPro,stockPro,imaPro);		
	}
	
	public double getTotalCompra(){
		return persistencia.getTotalCompra(usuari);
	}
	
	public void insertCistella(int codiP, String nomP, int quantitat, double preu) {
		persistencia.insertCistella(codiP, nomP, quantitat, preu, usuari);
	}
	
	public void eliminarProducte(String id) {
		persistencia.eliminarProducte(id);	
	}
	
	public void modificarProducte(int id, String nom, String descripcio, double preu, int estoc) {
		persistencia.modificarProducte(id, nom, descripcio, preu, estoc);
	}
	
	public int getCodiProducte(){
		return this.codiProducte;
	}
	
	public void setCodiProducte(int codi){
		this.codiProducte = codi;
	}
	
	public void buidarCistella(){
		persistencia.buidarCistella(usuari);
	}
	
	public void generarFactura(double total) {
		numFactura = persistencia.generarFactura(getCistella(), total, usuari);
	}
	
	public String[] getProducteConcret(int codi){
		return persistencia.getProducteConcret(codi);
	}
	
	public void buidarProducte(int codip) {
		persistencia.buidarProducte(codip);
	}
	
	public void setCercar(String cer) {
		this.cercar = cer;
	}

	public boolean hiEsCistella(int codiP) {
		return persistencia.hiEsCistella(codiP, usuari);
	}

	public void updateCistella(int codiP, int qtt, double preuProd) {
		persistencia.updateCistella(codiP, qtt, preuProd, usuari);
	}
}
