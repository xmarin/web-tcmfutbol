package Tenda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class TendaBBDD {
	
	//atributs connexi�
	private Connection con;  
	private Statement st;
	private ResultSet rs;
	private PreparedStatement ps;
	private ResultSetMetaData rsmd;

		
	public TendaBBDD(){
		connectarBBDD();
	}

	public void connectarBBDD(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost/tcmfutbol?user=root&password=");
		} catch (java.lang.ClassNotFoundException e) {
			System.err.print("ClassNotFoundException: "+  e.getMessage());
		} catch (SQLException ex) {
			System.out.println("SQLException caught" + ex.getMessage());
		}
	}
		
	private void desconnectarBBDD(){
		try {
			if (!con.isClosed()){
				rs.close();
				st.close();
				con.close();
			}
		} catch (SQLException ex) {
			System.out.println("Message:   " + ex.getMessage ());
		}
	}
	
	public String existeixUser(String user, String pass){
		try{
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT COUNT(*) FROM usuaris WHERE usuari='" + user +"' AND password='" + pass +"'");
			rs.next();
			if (rs.getInt(1) == 0) return "error";
			return "OK";

		} catch (SQLException ex){
			System.out.println("Error obtenint usuaris de la BBDD: " + ex.getMessage());
			return "error";
		}
	}	
	

	public HashMap<Integer,String[]> getProductesCercar(String cerca) throws Exception{
		try{
			HashMap<Integer,String[]> res = new HashMap<Integer,String[]>();
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM producte WHERE nom_producte LIKE '%"+cerca+"%'");
			while (rs.next()){
				String []prod = {rs.getString(2), 
						rs.getString(3), 
						rs.getString(4), 
						String.valueOf(rs.getDouble(5)), 
						String.valueOf(rs.getInt(6)),
						rs.getString(7).split(":")[0]	
					};
				res.put(rs.getInt(1), prod);
			}
			
			if (res.isEmpty()) return null;
			return res;
			
		} catch (SQLException ex){
			System.out.println("Error obtenint productes cercats de la BBDD: " + ex.getMessage());
			return null;
		}
	}
	
	
	public HashMap<Integer,String[]> getProductesPerFamilia(String familia) throws Exception{
		try{
			HashMap<Integer,String[]> res = new HashMap<Integer,String[]>();
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM producte WHERE nom_familia='" + familia +"'");
			while (rs.next()){
				String []prod = {rs.getString(2), 
						rs.getString(3), 
						rs.getString(4), 
						String.valueOf(rs.getDouble(5)), 
						String.valueOf(rs.getInt(6)),
						rs.getString(7).split(":")[0]	
					};
				res.put(rs.getInt(1), prod);
			}
			
			if (res.isEmpty()) return null;
			return res;
			
		} catch (SQLException ex){
			System.out.println("Error obtenint productes per familia de la BBDD: " + ex.getMessage());
			return null;
		}
	}

	public String insertUser(String nomReg, String cogReg, String usuReg,
			String passReg, String pobReg, String telReg, String nifReg, String emaReg){
		try{
			
			String sql = "INSERT INTO usuaris (NOM_CLIENT, COGNOM, USUARI, PASSWORD, POBLACIO, TELEFON, NIF, EMAIL, ADMIN)"
					+" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			PreparedStatement pst = this.con.prepareStatement(sql);
			pst.clearParameters();
			pst.setString(1,nomReg);
			pst.setString(2,cogReg);
			pst.setString(3,usuReg);
			pst.setString(4,passReg);
			pst.setString(5,pobReg);
			pst.setString(6,telReg);
			pst.setString(7,nifReg);
			pst.setString(8,emaReg);
			pst.setInt(9,0);
			pst.execute();
			return "OK";		

		} catch (SQLException ex){
			System.out.println("Error inserint usuari a la BBDD: " + ex.getMessage());
			return "error";
		}	
	}

	public HashMap<Integer, String[]> getCistella(String nomUsuari) {
		try{
			int codiUsuari = getCodiUsuari(nomUsuari);
			HashMap<Integer,String[]> res = new HashMap<Integer,String[]>();
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM cistella WHERE codi_usuari=" + codiUsuari);
			
			while (rs.next()){
				int codiP = rs.getInt(2);
				String nomP = rs.getString(3);
				double preuP = rs.getDouble(4);
				int qtt = rs.getInt(5);
				double preu = rs.getDouble(6);
				
				String []prod = {nomP, String.valueOf(preuP), String.valueOf(qtt), String.valueOf(preu),""};
				
				res.put(codiP, prod);
			}
			
			if (res.isEmpty()) return null;
		
			for (int p : res.keySet()){
				String imatge = getIMG(p);
				String parts[] = imatge.split(":");
				String []prod = res.get(p);
				prod[4] = parts[0];
			}
			return res;
			
		} catch (SQLException ex){
			System.out.println("Error obtenint cistella de la BBDD: " + ex.getMessage());
			return null;
		}
	}
	
	public double getTotalCompra(String nomUsuari) {
		try{
			int codiUsuari = getCodiUsuari(nomUsuari);
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT SUM(IMPORT) FROM cistella WHERE CODI_USUARI=" + codiUsuari);
			
			if (rs.next()) return rs.getDouble(1);
			return -1;
		} catch (SQLException ex){
			System.out.println("Error obtenint total cistella de la BBDD: " + ex.getMessage());
			return -1;
		}
	}
	
	
	private String getIMG(int codiProducte){
		try{
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT img FROM producte WHERE codi_producte='" + codiProducte +"'");
			
			if (rs.next())return rs.getString(1);
			
			return null;
		} catch (SQLException ex){
			System.out.println("Error obtenint img de la BBDD: " + ex.getMessage());
			return null;
		}
	}
	
	private int getCodiUsuari(String usuari){
		try{
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT codi_usuari FROM usuaris WHERE usuari='" + usuari +"'");
			
			if (rs.next())return rs.getInt(1);
		
			return -1;
		} catch (SQLException ex){
			System.out.println("Error obtenint codi usuari de la BBDD: " + ex.getMessage());
			return -1;
		}
	}

	public String esAdmin(String usuari) {

		try {
			if (con.isClosed()) connectarBBDD();
			String sql = "SELECT * FROM usuaris WHERE USUARI = ? AND ADMIN='1'";
			PreparedStatement pst;
			pst = this.con.prepareStatement(sql);
			pst.clearParameters();
			pst.setString(1, usuari);
			ResultSet resultat = pst.executeQuery();
			if (resultat.next()){
				return "OK";					
			}
			
		} catch (SQLException e) {
			System.out.println("Error obtenint admins: " + e.getMessage());
			return "error";
		}
		return "error";
		
	}

	public int insertProducte(String nomFam, String nomPro, String descPro,
			double preuPro, int stockPro, String imaPro) {
		try{
			if (con.isClosed()) connectarBBDD();

			String sql = "INSERT INTO producte (NOM_FAMILIA, NOM_PRODUCTE, DESCRIPCIO, PREU_PRODUCTE, STOCK, img)"
					+" VALUES(?, ?, ?, ?, ?, ?)";
			
			PreparedStatement pst = this.con.prepareStatement(sql);
			pst.clearParameters();
			
		
			pst.setString(1,nomFam);
			pst.setString(2,nomPro);
			pst.setString(3,descPro);
			pst.setDouble(4,preuPro);
			pst.setInt(5,stockPro);
			pst.setString(6,imaPro);
			pst.execute();
			
			st = con.createStatement();
			rs = st.executeQuery("SELECT codi_producte FROM producte WHERE nom_producte='" + nomPro +"'");
			
			if (rs.next())return rs.getInt(1);
			return -1;	

		} catch (SQLException ex){
			System.out.println("Error inserint producte a la BBDD: " + ex.getMessage());
			return -1;
		}	
	}

	public void insertCistella(int codiP, String nomP, int quantitat, double preu, String usuari) {
		try{
			String nomProducte = getNomProducte(codiP);
			int codiUsuari = getCodiUsuari(usuari);
			if (con.isClosed()) connectarBBDD();
			String sql = "INSERT INTO cistella (CODI_USUARI, CODI_PRODUCTE, NOM_PRODUCTE, PREU_PRODUCTE, QUANTITAT, IMPORT)"
					+" VALUES(?, ?, ?, ?, ?, ?)";
			
			PreparedStatement pst = this.con.prepareStatement(sql);
			pst.clearParameters();
			pst.setInt(1,codiUsuari);
			pst.setInt(2, codiP);
			pst.setString(3, nomProducte);
			pst.setDouble(4, preu);
			pst.setInt(5, quantitat);
			pst.setDouble(6, preu*quantitat);
			
			pst.execute();	

		} catch (SQLException ex){
			System.out.println("Error inserint producte a la cistella de la BBDD: " + ex.getMessage());
		}	
	}

	private String getNomProducte(int codiProducte){
		try{
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT nom_producte FROM producte WHERE codi_producte='" + codiProducte +"'");
			
			if (rs.next())return rs.getString(1);

			return "";
		} catch (SQLException ex){
			System.out.println("Error obtenint nom producte de la BBDD: " + ex.getMessage());
			return "";
		}
	}

	public ArrayList getFamiliasArrayList() {
		ArrayList res = new ArrayList();
		String consulta = "SELECT * FROM familia_producte";

		try {
			if (con.isClosed())connectarBBDD();
			ps = this.con.prepareStatement(consulta);
			rs = ps.executeQuery();
			rsmd = rs.getMetaData();
			ArrayList f;
			while (rs.next()) {
				f = new ArrayList();
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					f.add(rs.getString(i));
				}
				res.add(f);
			}
		} catch (SQLException ex) {
			System.out.println("Error Obtenint les families de productes de la BBDD:   " + ex.getMessage());
		}
		return res;
	}

	public ArrayList getProductesArrayList() {
		ArrayList res = new ArrayList();
		String consulta = "SELECT * FROM producte";

		try {
			if (con.isClosed())connectarBBDD();
			ps = this.con.prepareStatement(consulta);
			rs = ps.executeQuery();
			rsmd = rs.getMetaData();
			ArrayList f;
			while (rs.next()) {
				f = new ArrayList();
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					f.add(rs.getString(i));
				}
				res.add(f);
			}
		} catch (SQLException ex) {
			System.out.println("Error obtenint tots els productes de la BBDD:   " + ex.getMessage());
		}

		return res;
	}

	public void eliminarProducte(String id) {
		
		String consulta = "DELETE FROM producte WHERE codi_producte='"+id+"'";
		try {
			if (con.isClosed())connectarBBDD();
			st = this.con.createStatement();
			st.execute(consulta);
		} catch (SQLException ex) {
			System.out.println("Error al eliminar un producte de la BBDD:   " + ex.getMessage());
		}
	}

	public void modificarProducte(int id, String nomProducte, String descripcio, double preu, int estoc) {
		try{		
			if (con.isClosed()) connectarBBDD();
			String sql = "UPDATE producte SET nom_producte='" + nomProducte + "', descripcio='" + descripcio + "', "
					+ "preu_producte='" + preu + "', stock='"+estoc+"' WHERE codi_producte=" + id;
			st = this.con.createStatement();
			st.execute(sql);

		} catch (SQLException ex){
			System.out.println("Error al modificar el producte: " + ex.getMessage());
		}	
	}

	public void buidarCistella(String usuari) {
		int codi = getCodiUsuari(usuari);
		String consulta = "DELETE FROM cistella WHERE codi_usuari='"+codi+"'";
		try {
			if (con.isClosed())connectarBBDD();
			st = this.con.createStatement();
			st.execute(consulta);

		} catch (SQLException ex) {
			System.out.println("Error buidant cistella de la BBDD:   " + ex.getMessage());
		}
	}

	public int generarFactura(HashMap<Integer, String[]> cistella, double total, String usuari) {
		try{
			if (con.isClosed()) connectarBBDD();
			
			int codiUsuari = getCodiUsuari(usuari);
			int idFactura = obtenirNumFactura() + 1;
			
			if (idFactura == 0) idFactura = 1; 
			String sql = "INSERT INTO factura (ID_FACTURA, CODI_USUARI, DATA, IMPORT_FACTURA)"
					+" VALUES(" + idFactura + "," + codiUsuari + " ,CURRENT_DATE," + total + ")";
			st = this.con.createStatement();
			st.execute(sql);
			
			int num = 1;
			for (int codi : cistella.keySet()){
				String prod[] = cistella.get(codi);
				String sql2 = "INSERT INTO linia_factura (NUM_LINIA, ID_FACTURA, CODI_PRODUCTE, PREU)"
						+" VALUES(" + num + "," + idFactura + ", " + codi + ", "+ prod[1] +")";
				st = this.con.createStatement();
				st.execute(sql2);
				
				String sql3 = "UPDATE producte SET stock = stock - " + prod[2] + " WHERE codi_producte =" + codi;
				st = this.con.createStatement();
				st.execute(sql3);
				
				num++;
			}
			
			buidarCistella(usuari);
			return idFactura;
		
		} catch (SQLException ex){
			System.out.println("Error generant factura a la BBDD: " + ex.getMessage());
			return -1;
		}	
	}
	
	private int obtenirNumFactura(){
		try{
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT id_factura FROM factura ORDER BY id_factura DESC limit 1");
			if (rs.next()) return rs.getInt(1);

			return -1;
			
		} catch (SQLException ex){
			System.out.println("Error obtenint numfactura de la BBDD: " + ex.getMessage());
			return -1;
		}
	}

	public String[] getProducteConcret(int codi) {
		try{
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM producte WHERE codi_producte=" + codi);
			if (rs.next()){
				String [] res = {rs.getString(1),
						rs.getString(2), 
						rs.getString(3), 
						rs.getString(4), 
						String.valueOf(rs.getDouble(5)), 
						String.valueOf(rs.getInt(6)),
						rs.getString(7).split(":")[0]	
					};
				
				return res;
			}
			return null;
			
		} catch (SQLException ex){
			System.out.println("Error obtenint producte concret de la BBDD: " + ex.getMessage());
			return null;
		}
	}
	
	public void buidarProducte(int codip) {
		String consulta = "DELETE FROM cistella WHERE codi_producte='"+codip+"'";
		try {
		   if (con.isClosed())connectarBBDD();
		   st = this.con.createStatement();
		   st.execute(consulta);

		  } catch (SQLException ex) {
		   System.out.println("Error buidant producte de la BBDD:   " + ex.getMessage());
		  }
	}

	public boolean hiEsCistella(int codiP, String usuari) {
		try{
			int codiUsuari = getCodiUsuari(usuari);
			if (con.isClosed()) connectarBBDD();
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM cistella WHERE codi_producte=" + codiP +" AND codi_usuari = '" + codiUsuari + "'");
			if (rs.next()) return true;
			return false;
			
		} catch (SQLException ex){
			System.out.println("Error obtenint si el producte esta a la cistella de la BBDD: " + ex.getMessage());
			return false;
		}
	}

	public void updateCistella(int codiP, int qtt, double preuProd, String usuari) {
		try{		
			int codiUser = getCodiUsuari(usuari);
			if (con.isClosed()) connectarBBDD();
			String sql = "UPDATE cistella SET quantitat = quantitat + " + qtt + ", import = import + " 
			+ preuProd*qtt + " WHERE codi_usuari = " + codiUser + " AND codi_producte = " + codiP;
			st = this.con.createStatement();
			st.execute(sql);

		} catch (SQLException ex){
			System.out.println("Error al modificar el producte de la cistella: " + ex.getMessage());
		}	
	}
}