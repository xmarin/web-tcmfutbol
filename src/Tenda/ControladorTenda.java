package Tenda;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/ControladorTenda")
public class ControladorTenda extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String[] missatges = { "L'usuari ja existeix",
			"Camp usuari: caracters incorrectes." ,
	"Camp contrasenya: caracters incorrectes."};


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/html;charset=ISO-8859-1");
		request.setCharacterEncoding("ISO-8859-1");

		HttpSession session = request.getSession();
		BeanProducte bean = (BeanProducte) session.getAttribute("bean");
		if (bean == null){
			bean = new BeanProducte();
			session.setAttribute("bean", bean);
		}


		String accio = request.getParameter("accio");
		switch(accio){
		case "login":
			bean = (BeanProducte) session.getAttribute("bean");
			bean.setUsuari(request.getParameter("user"));
			bean.setPassword(request.getParameter("pass"));
			session.setAttribute("user", request.getParameter("user"));

			if (bean.existeixUser(bean.getUsuari(), bean.getPassword())) {
				if(bean.esAdmin(bean.getUsuari())){
					session.setAttribute("admin", "si");
				}else{
					session.setAttribute("admin", null);
				}
				this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
			} else {
				bean.setUsuari(null);
				bean.setPassword(null);
				session.setAttribute("user", null);
				this.getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
			}
			break;
		case "registre":
			String nomReg = request.getParameter("nom");
			String cogReg = request.getParameter("cog");
			String usuReg = request.getParameter("usu");
			String passReg = request.getParameter("pass");
			String pobReg = request.getParameter("pob");
			String telReg = request.getParameter("tel");
			String nifReg = request.getParameter("nif");
			String emaReg = request.getParameter("ema");


			boolean repetit = false;
			if (bean.existeixUser(usuReg, passReg)){
				repetit = true;
			}

			if (repetit)response.sendRedirect("login.jsp?msg=" + missatges[0]);
			else {
				String res = validarCamps(nomReg,cogReg,usuReg,passReg,pobReg,telReg,nifReg,emaReg);
				if (res.equals("OK")){
					if (bean.insertUser(nomReg,cogReg,usuReg,passReg,pobReg,telReg,nifReg,emaReg)){
						session.setAttribute("user", usuReg);
						response.sendRedirect("index.jsp");
					} else {
						response.sendRedirect("login.jsp");
					}
				}else{
					response.sendRedirect("registre.jsp?msg="+res);
				}
			}
			break;

		case "logout":
			response.sendRedirect("logout.jsp");
			break;

		case "adminAfegir":
			String nomFam = request.getParameter("nomFamilia");
			String nomPro = request.getParameter("nomProducte");
			String descPro = request.getParameter("descProducte");
			double preuPro = Double.parseDouble(request.getParameter("preuProducte"));
			int stockPro = Integer.parseInt(request.getParameter("stockProducte"));
			String imaPro = request.getParameter("imatgeProducte");

			if(nomFam.equals("Samarretes")) imaPro = "img/shirts/"+imaPro;
			else if(nomFam.equals("Pilotes")) imaPro = "img/balls/"+imaPro;
			else if(nomFam.equals("Porter")) imaPro = "img/gkp/"+imaPro;
			else if(nomFam.equals("Botes")) imaPro = "img/bots/"+imaPro;
			else if(nomFam.equals("Complements")) imaPro = "img/cmpl/"+imaPro;
			
			int codiProd = bean.insertProducte(nomFam, nomPro, descPro, preuPro, stockPro, imaPro);
			bean.setCodiProducte(codiProd);
			response.sendRedirect("product-details.jsp");
			break;

		case "adminEsborrar":
			String id = (String)request.getParameter("el");
			bean.eliminarProducte(id);
			response.sendRedirect("adminEliminar.jsp");
			break;
			
		case "adminModificar":
			int idProd = Integer.parseInt(request.getParameter("id"));
			String nom = request.getParameter("nom");
			String descripcio = request.getParameter("des");
			double preu = Double.parseDouble(request.getParameter("preu"));
			int estoc = Integer.parseInt(request.getParameter("stock"));
			bean.modificarProducte(idProd, nom, descripcio, preu, estoc);
			bean.setCodiProducte(idProd);
			response.sendRedirect("product-details.jsp");
			break;
		
		case "productes":
			String familia = request.getParameter("familia");
			bean.setFamilia(familia);
			response.sendRedirect("shop.jsp");
			break;
			
		case "addCistella":
			bean.setUsuari((String)session.getAttribute("user"));
			int codiP = Integer.parseInt(request.getParameter("codiP"));
			String nomP = request.getParameter("nomP");
			int qtt = Integer.parseInt(request.getParameter("qtt"));
			double preuProd = Double.parseDouble(request.getParameter("total"));

			if (bean.hiEsCistella(codiP)) bean.updateCistella(codiP, qtt, preuProd);
			else bean.insertCistella(codiP, nomP, qtt, preuProd);
			response.sendRedirect("cart.jsp");
			break;
			
		case "buidarCistella":
			bean.buidarCistella();
			response.sendRedirect("cart.jsp");
			break;
			
		case "buidarProducte":
			int codip = Integer.parseInt(request.getParameter("producte"));
			bean.buidarProducte(codip);
			response.sendRedirect("cart.jsp");
			break;
			
		case "detalls":
			int codi = Integer.parseInt((String)request.getParameter("prod"));
			bean.setCodiProducte(codi);
			response.sendRedirect("product-details.jsp");
			break;
			
		case "cercar":
			String cer = request.getParameter("cercar");
			bean.setCercar(cer);
			response.sendRedirect("cercar.jsp");
			break;
			
		case "pagar":
			double totalFactura = Double.parseDouble(request.getParameter("total"));
			bean.generarFactura(totalFactura);
			response.sendRedirect("confirm.jsp");	
			break;
		}

	}
	private String validarCamps(String nom, String cognom, String usuari, String pass, String poblacio, String telf, String nif, String email){

		int i=0, j=0;
		boolean errorUser = false, errorPass = false;
		while (i < usuari.length() && !errorUser) {
			if (!Character.isLetter(usuari.charAt(i)) && !Character.isDigit(usuari.charAt(i))) errorUser = true;
			i++;
		}

		while (j < pass.length() && !errorUser && !errorPass){
			if (!Character.isLetter(pass.charAt(j)) && !Character.isDigit(pass.charAt(j))) errorPass = true;
			j++;
		}


		if (errorUser) return missatges[8];
		else if (errorPass) return missatges[9];
		else return "OK";
	}
}
