<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="bean" scope="session" class="Tenda.BeanProducte"/>
<%if(session.getAttribute("admin")==null){response.sendRedirect("login.jsp");} %>
<!DOCTYPE html>
<%String par = request.getParameter("producte");
String []prod = bean.getProducteConcret(Integer.parseInt(par));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modificació producte</title>
</head>
<body>
	<div class="login-form" style="width:30%"><!--login form-->
		<form action="ControladorTenda" method="get"> 
			<input type="hidden" name="accio" value="adminModificar">
			<label>Codi producte:</label><input STYLE="text-align: center" type='text' value="<%out.println(prod[0]);%>" name='id' readonly="readonly"/>
			<label>Familia de producte:</label><input STYLE="text-align: center" type='text' value="<%out.println(prod[1]);%>" name='fam' readonly="readonly"/>
			<label>Nom producte:</label><input STYLE="text-align: center" type='text' value="<%out.println(prod[2]);%>" name='nom' required="required"/>
			<label>Descripció:</label><input STYLE="text-align: center" type='text' value="<%out.println(prod[3]);%>" name='des' required="required"/>
			<label>Preu:</label><input STYLE="text-align: center" type="text" value="<%out.println(prod[4]);%>" name='preu'/>
			<label>Estoc:</label><input STYLE="text-align: center" type='text' value="<%out.println(prod[5]);%>" name='stock'/>
			<label>Imatge:</label><input STYLE="text-align: center" type='text' value="<%out.println(prod[6]);%>" name='img'/>
			<button type="submit" class="btn btn-default">Modificar</button>	
		</form>
	</div>
</body>
</html>