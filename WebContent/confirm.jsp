<%@ page import="java.util.*" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="bean" scope="session" class="Tenda.BeanProducte"/>
<%if(session.getAttribute("user")==null){response.sendRedirect("login.jsp");} %>
<!DOCTYPE html>
<html>
<head>
    <title>Compra finalitzada | TCM FUTBOL</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="img/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +931234567</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> tcmfutbol@gmail.com</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.jsp"><img src="img/home/logo.png" alt="" /></a>
						</div>
							
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<%
								if (session.getAttribute("user") == null) {
									out.println("<li><a href='login.jsp'><i class='fa fa-lock'></i> Login</a></li>");
								}else{
									out.println("<li><a><i class='fa fa-user'></i>Benvingut "+session.getAttribute("user")+"</a></li>");
									out.println("<li><a href='cart.jsp'><i class='fa fa-shopping-cart'></i>Cistella</a></li>");
									out.println("<li><a href='ControladorTenda?accio=logout'><i class='fa fa-lock'></i> Logout</a></li>");
								}
								%>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.jsp" class="active">Inici</a></li>
								<li class="dropdown"><a href="#"> Categories<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="ControladorTenda?accio=productes&familia=Samarretes">Samarretes</a></li>
										<li><a href="ControladorTenda?accio=productes&familia=Botes">Botes</a></li> 
										<li><a href="ControladorTenda?accio=productes&familia=Pilotes">Pilotes</a></li> 
										<li><a href="ControladorTenda?accio=productes&familia=Porter">Porters</a></li> 
										<li><a href="ControladorTenda?accio=productes&familia=Complements">Complements</a></li> 
                                    </ul>
                                </li>
								<%if (session.getAttribute("admin")!=null) {%>
								<li class="dropdown"><a href="#"> Administrador<i
										class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
										<li><a
											href="adminAfegir.jsp">Afegir producte</a></li>
										<li><a
											href="adminLlegir.jsp">Llegir producte</a></li>											
										<li><a
											href="adminEliminar.jsp">Esborrar producte</a></li>
										<li><a
											href="adminModificar.jsp">Modificar producte</a></li>
									</ul>
								</li>
								<%}%>
								<li><a href="contacte.jsp">Contacte</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<form action="ControladorTenda?accio=cercar" method="post"> 
								<input type="text" name="cercar" placeholder="Cercar" />
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header>
	 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center">Confirmaci�</h2>    			    				    				
				</div>			 		
			</div>    	
    		<div class="row">  	
	    		
	    		<div class="col-sm-4" style="width: 100%">
	    			<div class="contact-info">
	    					<img alt="" src="img/home/stick.jpg" style="width: 160px; float: left; padding-right: 40px">
	    					<br><p>Gracies <%session.getAttribute("user");%> per realitzar la seva compra a TCMFUTBOL.</p><br>
	    					<p>El seu identificador de factura es: ${bean.numFactura}</p><br><br><br>
	    					<a class="btn btn-default check_out" href="index.jsp">Tornar a l'�ndex</a>
	    					<br><br><br>
	    			</div>
    			</div>   			
	    	</div>  
    	</div>	
    	
    </div><!--/#contact-page-->

	<footer id="footer">
		<!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2>
								<span>TCM</span>FUTBOL
							</h2>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Serveis</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="contacte.jsp">Contacte</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="img/home/map.png" alt="" />
							<p>Cam� Ral n�701, Matar�(Espanya)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright � 2016 TCM FUTBOL.</p>
				</div>
			</div>
		</div>

	</footer>
	<!--/Footer-->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
</body>
</html>