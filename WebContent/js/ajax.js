function loadProd(){
	var xmlhttp;

	var e = document.getElementById("comboProductes");
    var productes = e.options[e.selectedIndex].value;
	
	if (window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();// code for IE7+, Firefox, Chrome, Opera, Safari
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
	}
	
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4){
			document.getElementById("modificar").innerHTML=xmlhttp.responseText;
  		}
	}
	xmlhttp.open("GET","modificar.jsp?producte=" + productes,true);
	xmlhttp.send();
}